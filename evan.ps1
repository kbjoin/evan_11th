
$Decryption_chars = @("0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f",
                      "g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v",
                      "x","x","y","z","A","B","C","D","E","F","G","H","I","J","K","L",
                      "M","N","O","P","Q","R","S","T","U","V","X","X","Y","Z","!","@",
                      "#","$","%","^","&","*","(",")","_","=","+","[","]","{","}","\",
                      "|","*","+","-")


function PrintAgentTophat
{
Write-Host "
   .------\ /------.
   |       -       |
   |               |
   |               |
   |               |
_______________________
===========.===========
  / ~~~~~     ~~~~~ \
 / |    |    |    |  \
 W  ----  / \ ----   W
 \.      |o o|      ./
  |                 |
  \    #########    /     $args
   \  ## ----- ##  /
    \##         ##/
     \_____v_____/" -ForegroundColor Green
}

#----------- MAIN THREAD START -----------#
Clear-Host
PrintAgentTophat "Good, you found my message. Verification Needed."
Read-Host "Full Name"
Read-Host "Autherization Code"
Clear-Host
PrintAgentTophat "Hmmm..."
Start-Sleep 2
Start-Process microsoft.windows.camera:
Start-Sleep 10
taskkill -F -IM "WindowsCamera.exe" /T
Clear-Host
PrintAgentTophat "Good. I thought it was you but I had to be sure."
Start-Sleep 5
Clear-Host
PrintAgentTophat "Sending you the file now, Decryption will start on download..."

$sleep_start = 0
$sleep_stop = 100
Write-Host "Download in progress.. $sleep_start%" -NoNewline
while ($sleep_start -lt $sleep_stop)
{
    start-sleep 1
    $sleep_start = $sleep_start +10
    write-host "...$sleep_start%" -NoNewline
}
Write-Host ""
Write-Host "Download Complete. Starting Decryption Process..."
Start-Sleep 2
Clear-Host

$i = 0 
$i_max = 8192
while ($i -lt $i_max)
{
    if (($i % 64 -ne 0) -or ($i -eq 0))
    {
        write-host ($Decryption_chars | Get-Random) -NoNewline
    }
    else
    {
        write-host ($Decryption_chars | Get-Random)
    }
    $i++
}


Write-Host ""
Write-Host "---DECRYPTION ERROR---
- SECRET KEY INVALID -" -ForegroundColor DarkRed
start-sleep 4
Clear-Host
PrintAgentTophat "something is wrong, I need your secret code to break the last layer"
$sc = read-host "Secret Code"
clear-host
PrintAgentTophat "Let's hope this works..."
start-sleep 5
Clear-Host
$i = 0 
$i_max = 8192
$colors = @("Blue","Green","Cyan","Red","Magenta","Yellow","White")
while ($i -lt $i_max)
{
    $color = ($colors | Get-Random)
    if (($i % 64 -ne 0) -or ($i -eq 0))
    {
        write-host ($Decryption_chars | Get-Random) -NoNewline -ForegroundColor $color
    }
    else
    {
        write-host ($Decryption_chars | Get-Random) -ForegroundColor $color
    }
    $i++
}
start-sleep 2
Clear-Host
PrintAgentTophat "Got it!"
Start-Sleep 5
Clear-Host
PrintAgentTophat "The Message appears to be, Happy Birthday! Enjoy your new game!"
Start-Sleep 5
Start-Process "https://store.steampowered.com/app/1089980/The_Henry_Stickmin_Collection/"
start-sleep 60
